// db/index.js

const { Pool } = require('pg')
const config = require('../cfg/config.js');


var dbConfig = {
  user: config.PGUSER,
  database: config.PGDATABASE,
  host: config.PGHOST,
  password: config.PGPASSWORD,
  port: config.PGPORT,
  max: config.MAX_CONN
};

// console.log("config : "+JSON.stringify(config));

const pool = new Pool(dbConfig)
pool.query("SET application_name = Node_PG_Backend", (err, res)=>{
  if(err){
      console.log("DB Connection Error");
  }
})

// the pool with emit an error on behalf of any idle clients
// it contains if a backend error or network partition happens
pool.on('error', (err, client) => {
  console.error('Unexpected error on idle client', err)
  process.exit(-1)
})

module.exports = {
  query: (text, params, callback) => {
    const start = Date.now()
    return pool.query(text, params, (err, res) => {
      const duration = Date.now() - start
      console.log('executed query', { text, duration, rows: res.rowCount })
      callback(err, res)
    })
  }
}