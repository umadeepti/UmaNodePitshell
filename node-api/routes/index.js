// routes/index.js

const piRoutes      = require('./pi_routes');

module.exports = function(app, db) {
  piRoutes(app, db);
  // Other route groups could go here, in the future
};