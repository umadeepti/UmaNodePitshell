// routes/pi_routes.js

const pg            = require('../db');

module.exports = function(app, db) {

    app.get('/pirs', (request, response) => {
        // You'll create your priority item here.
        const text = 'SELECT * FROM pitmaster.priorityitem'

        pg.query(text, (err, res) => {
            console.log(err, res)
            response.send(res)
        })
    });

    // Return a selected item
    app.get('/pirs/:id', (request, response) => {
        // You'll create your priority item here.
        const id = request.params.id;
        const text = 'Select * from pitmaster.priorityitem where id=$1'
        const values = [id]

        pg.query(text, values, (err, res) => {
            console.log(err, res)
            response.send(res)
        })
    });
};