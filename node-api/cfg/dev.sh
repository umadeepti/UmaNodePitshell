#!/bin/bash
# port to start node server on
PORT=3000
export PORT

# postgres db connection string
# "postgres://postgres:postgres@localhost:5433/pitdb",
PGUSER="pituser"
export PGUSER
PGDATABASE="pitdb"
export PGDATABASE
PGHOST="localhost"
export PGHOST
PGPASSWORD=""
export PGPASSWORD
PGPORT=5433
export PGPORT

MAX_CONN=10
export MAX_CONN
