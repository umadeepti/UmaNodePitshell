"use strict";

!(function() {
    var key = ''
    ;

    const config = {
      PORT: process.env.PORT,
      PGUSER: process.env.PGUSER,
      PGDATABASE: process.env.PGDATABASE,
      PGHOST: process.env.PGHOST,
      PGPASSWORD: process.env.PGPASSWORD,
      PGPORT: process.env.PGPORT,
      MAX_CONN: process.env.MAX_CONN
    };
     
    console.log('*** running config start ***');
    for (key in config) {
        console.log(key + ': ' + config[key]);
    }
    console.log('*** running config end ***');

    module.exports = config;
})();
