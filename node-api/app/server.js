// server.js

const express       = require('express');
const pg            = require('pg');
const bodyParser    = require('body-parser');
const app           = express();
const config        = require('../cfg/config');

const port = config.PORT
app.use(bodyParser.urlencoded({ extended: true }));

require('../routes')(app, {});

app.listen(port, () => {
  console.log('We are live on ' + port);
});